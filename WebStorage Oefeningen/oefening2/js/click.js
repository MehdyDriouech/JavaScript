class Circle {}

class Game {
  createCircles() {}

  randomizeCircles() {}

  checkClick(x, y) {}

  checkEndGame() {}
}

class GameComponent {
  constructor(window) {
    this.window = window;
    this.cv = window.document.getElementById('gameCanvas');
    this.ctx = this._cv.getContext('2d');
    this.game = new Game(this._cv.width, this._cv.height);
    this.timer = null;
    this.record = 0;
    this.storage = window.localStorage;
  }

  get window() {
    return this._window;
  }
  get cv() {
    return this._cv;
  }
  get ctx() {
    return this._ctx;
  }
  get game() {
    return this._game;
  }
  get timer() {
    return this._timer;
  }
  get record() {
    return this._record;
  }
  get storage() {
    return this._storage;
  }

  set window(value) {
    this._window = value;
  }
  set cv(value) {
    this._cv = value;
  }
  set ctx(value) {
    this._ctx = value;
  }
  set game(value) {
    this._game = value;
  }
  set timer(value) {
    this._timer = value;
  }
  set record(value) {
    this._record = value;
  }
  set storage(value) {
    this._storage = value;
  }

  drawGame() {}

  drawCircles() {}

  showNumberRedBalls() {
    document.getElementById('rb').innerHTML = this._game.numberOfRedBalls;
  }

  startChronometer() {
    this._timer = this._window.setTimeout(() => {
      // -	de property numberOfSeconds van Game met 1 wordt verhoogd
      // -	er wordt gebruik gemaakt van de functie formatTime om dit te formatteren
      // -	het resultaat van formatTime wordt weergegeven op het scherm in het element met id time.
      // Maak hiervoor gebruik van innerHTML
      this.startChronometer();
    }, 1000);
  }

  stopChronometer() {
    this._timer = this._window.clearTimeout(this._timer);
  }

  stopGame() {}

  getRecordFromStorage() {}

  setRecordInStorage() {}

  formatTime(time) {}
}

function init() {}

window.onload = () => {
  init();
};
